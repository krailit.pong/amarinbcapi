import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import App.Controller.ApiMobileController;
import App.Service.ServiceStellar;
import App.function.Everything;

import static org.hamcrest.Matchers.is;

// @RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(classes = { TestContext.class, WebApplicationContext.class })
// @WebAppConfiguration
@RunWith(MockitoJUnitRunner.class)
public class ApiMobileControllerTest {

    // @Autowired
    // private MockMvc mockMvc;

    // @Spy
    @Mock
    private ApiMobileController apiMobileControllerMock;

    // @Spy
    @Mock
    private Everything everything;

    @Test
    public void testEverthing() {
        when(everything.test()).thenReturn("test");
        Assert.assertEquals("SUCCESS", everything.test(), "test");
    }
    

    @Test
    public void testCreateWallet() {
        // ApiMobileController apiMobileControllerMock = Mockito.mock(ApiMobileController.class);
        /* when(todoServiceMock.findById(1L)).thenThrow(new TodoNotFoundException(""));
        
        mockMvc.perform(get("/api/todo/{id}", 1L))
                .andExpect(status().isNotFound());
        
        verify(todoServiceMock, times(1)).findById(1L);
        verifyNoMoreInteractions(todoServiceMock); */

     /*    MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/createWalletById", ""
        ).contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .andExpect(status().is(HttpStatus.CREATED.value()))).andReturn(); */
               
        // String result = apiMobileControllerMock.createWalletById("");
        // verify(apiMobileControllerMock, times(1)).createWalletById("{\"_id\" : \"119\"}");
        
        // verifyNoMoreInteractions(everything);
        when(apiMobileControllerMock.createWalletById("{\"_id\" : \"119\"}")).thenReturn("{\"message\":\"success\",\"status\":true}");
        Assert.assertEquals("Failed ", apiMobileControllerMock.createWalletById("{\"_id\" : \"119\"}"), "{\"message\":\"success\",\"status\":true}");
    }
}