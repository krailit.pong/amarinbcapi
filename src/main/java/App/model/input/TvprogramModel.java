/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.model.input;

/**
 *
 * @author Nonbody
 */
public class TvprogramModel {
    private String program_name;
    private String fb_id;
    private String line;
    private String img;
    private String tvprogram_id;
    private String str_live_time = "";
    private String end_live_time = "";
    
    private String str_re_time = "";
    private String end_re_time = "";
    
    private String str_tap_time = "";
    private String end_tap_time = "";
    
    private String[] live_day = {};
    private String[] re_day = {};
    private String[] tap_day = {};

    public String getProgram_name() {
        return program_name;
    }

    public void setProgram_name(String program_name) {
        this.program_name = program_name;
    }

    public String getFb_id() {
        return fb_id;
    }

    public void setFb_id(String fb_id) {
        this.fb_id = fb_id;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

  

    public String getStr_live_time() {
        return str_live_time;
    }

    public void setStr_live_time(String str_live_time) {
        this.str_live_time = str_live_time;
    }

    public String getEnd_live_time() {
        return end_live_time;
    }

    public void setEnd_live_time(String end_live_time) {
        this.end_live_time = end_live_time;
    }

    public String[] getLive_day() {
        return live_day;
    }

    public void setLive_day(String[] live_day) {
        this.live_day = live_day;
    }

    public String[] getRe_day() {
        return re_day;
    }

    public void setRe_day(String[] re_day) {
        this.re_day = re_day;
    }

    public String[] getTap_day() {
        return tap_day;
    }

    public void setTap_day(String[] tap_day) {
        this.tap_day = tap_day;
    }

    public String getStr_re_time() {
        return str_re_time;
    }

    public void setStr_re_time(String str_re_time) {
        this.str_re_time = str_re_time;
    }

    public String getEnd_re_time() {
        return end_re_time;
    }

    public void setEnd_re_time(String end_re_time) {
        this.end_re_time = end_re_time;
    }

    public String getStr_tap_time() {
        return str_tap_time;
    }

    public void setStr_tap_time(String str_tap_time) {
        this.str_tap_time = str_tap_time;
    }

    public String getEnd_tap_time() {
        return end_tap_time;
    }

    public void setEnd_tap_time(String end_tap_time) {
        this.end_tap_time = end_tap_time;
    }

    public String getTvprogram_id() {
        return tvprogram_id;
    }

    public void setTvprogram_id(String tvprogram_id) {
        this.tvprogram_id = tvprogram_id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

   
    
    
}
