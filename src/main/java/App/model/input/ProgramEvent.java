/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.model.input;

/**
 *
 * @author Nonbody
 */
public class ProgramEvent {
    private String tvprogram_id;
    private String content_id;
    private String img;
    private String ep_name;
    private String ep_date;
    private String ep_type;
    private String ep_start_time;
    private String ep_end_time;

    public String getTvprogram_id() {
        return tvprogram_id;
    }

    public void setTvprogram_id(String tvprogram_id) {
        this.tvprogram_id = tvprogram_id;
    }

    public String getEp_name() {
        return ep_name;
    }

    public void setEp_name(String ep_name) {
        this.ep_name = ep_name;
    }

    public String getEp_date() {
        return ep_date;
    }

    public void setEp_date(String ep_date) {
        this.ep_date = ep_date;
    }

    public String getEp_type() {
        return ep_type;
    }

    public void setEp_type(String ep_type) {
        this.ep_type = ep_type;
    }

    public String getEp_start_time() {
        return ep_start_time;
    }

    public void setEp_start_time(String ep_start_time) {
        this.ep_start_time = ep_start_time;
    }

    public String getEp_end_time() {
        return ep_end_time;
    }

    public void setEp_end_time(String ep_end_time) {
        this.ep_end_time = ep_end_time;
    }

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
    
   
}
