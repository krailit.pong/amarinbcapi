/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.model.input;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Nonbody
 */
public class ContenttypeModel {
    @SerializedName("event_id")
    private String even_id;
    
    @SerializedName("type")
    private String type;

    public String getEven_id() {
        return even_id;
    }

    public void setEven_id(String even_id) {
        this.even_id = even_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
}
