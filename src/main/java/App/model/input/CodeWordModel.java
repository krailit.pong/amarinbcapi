/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.model.input;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Nonbody
 */
public class CodeWordModel {
    @SerializedName("event_id")
    private String even_id;
    
    @SerializedName("codeword")
    private int codeword;

    public String getEven_id() {
        return even_id;
    }

    public void setEven_id(String even_id) {
        this.even_id = even_id;
    }

    public int getCodeword() {
        return codeword;
    }

    public void setCodeword(int codeword) {
        this.codeword = codeword;
    }
    
    
}
