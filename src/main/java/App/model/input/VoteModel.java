/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.model.input;

/**
 *
 * @author Nonbody
 */
public class VoteModel {
    private String name_vote;
    private String des_vote;
    private String date_vote;
    private String point_vote;
    private String choice_1;
    private String choice_2;
    private String content_id;
    private String images_1;
    private String images_2;
    private String content_id_byvote;

    public String getName_vote() {
        return name_vote;
    }

    public void setName_vote(String name_vote) {
        this.name_vote = name_vote;
    }

    public String getDes_vote() {
        return des_vote;
    }

    public void setDes_vote(String des_vote) {
        this.des_vote = des_vote;
    }

    public String getDate_vote() {
        return date_vote;
    }

    public void setDate_vote(String date_vote) {
        this.date_vote = date_vote;
    }

    public String getPoint_vote() {
        return point_vote;
    }

    public void setPoint_vote(String point_vote) {
        this.point_vote = point_vote;
    }

    public String getChoice_1() {
        return choice_1;
    }

    public void setChoice_1(String choice_1) {
        this.choice_1 = choice_1;
    }

    public String getChoice_2() {
        return choice_2;
    }

    public void setChoice_2(String choice_2) {
        this.choice_2 = choice_2;
    }

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public String getImages_1() {
        return images_1;
    }

    public void setImages_1(String images_1) {
        this.images_1 = images_1;
    }

    public String getImages_2() {
        return images_2;
    }

    public void setImages_2(String images_2) {
        this.images_2 = images_2;
    }

    public String getContent_id_byvote() {
        return content_id_byvote;
    }

    public void setContent_id_byvote(String content_id_byvote) {
        this.content_id_byvote = content_id_byvote;
    }
  
    
}
