package App.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import App.RestConfiguration;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Service
public class RepositoryWallet implements RepositoryInterfaceWallet {

    @Autowired
    RestConfiguration restConfiguration;

    @Autowired
    private JedisCluster jedis;

    @Override
    public void savewalletId(String walletId, String status) {
        jedis.set(walletId, status);
    }

    @Override
    public String getwalletId(String walletId) {
        return jedis.get(walletId);
    }
}