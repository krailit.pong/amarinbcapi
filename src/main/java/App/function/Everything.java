/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.function;

import App.Controller.ApiMobileController;
import App.Service.ServiceLockboxInterface;
import redis.clients.jedis.JedisCluster;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.Strings;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Nonbody
 */
@Service
public class Everything {

    @Value("${path.input.img}")
    private String path_input_img;

    @Value("${path.get.img}")
    private String path_get_img;

    @Value("${es_event}")
    private String es_event;

    @Autowired
    private RestService rest;

    @Autowired
    private FormatSerch search;

    @Autowired
    private ServiceLockboxInterface Service_Lockbox;

    @Autowired
    private JedisCluster jedis;

    public String renameEp(String activityId) {
        String name = activityId + "Ep";
        return Strings.padStart(name, 12, '0');
    }

    public String md5(String message) {
        String digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hash = md.digest(message.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder(2 * hash.length);
            for (byte b : hash) {
                sb.append(String.format("%02x", b & 0xff));
            }
            digest = sb.toString();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Everything.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Everything.class.getName()).log(Level.SEVERE, null, ex);
        }
        return digest;
    }

    public String format_bydate() {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String date = dt.format(new Date());

        return date;
    }

    public String format_evdate(String date_f) {
        String date = "";
        try {
            SimpleDateFormat d1 = new SimpleDateFormat("MM/dd/yyyy");
            Date data = d1.parse(date_f);
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            date = dt.format(data);

        } catch (ParseException ex) {
            Logger.getLogger(Everything.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }

    public String input_file(MultipartFile file) {
        String path_get = "";
        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(path_input_img + file.getOriginalFilename());
            Files.write(path, bytes);
            path_get = path_get_img + file.getOriginalFilename();
        } catch (IOException ex) {
            Logger.getLogger(ApiMobileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return path_get;
    }

    public JSONObject format_tvprogram(String st_time, String ed_time, String[] date_all) {
        JSONObject obj = new JSONObject();
        JSONArray ary_date = new JSONArray();
        boolean check = false;
        if (date_all.length > 0) {
            check = true;
            for (int i = 0; i < date_all.length; i++) {
                ary_date.put(new JSONObject().put("day", date_all[i]));
            }
        }
        obj.put("status", check);
        obj.put("start_time", st_time);
        obj.put("end_time", ed_time);
        obj.put("date", ary_date);
        ary_date = null;

        return obj;
    }

    public int get_episode(String id) {
        int c = 0;
        c = rest.post(search.get_episode(id), es_event + "/_search").getJSONObject("hits").getInt("total");

        return c;
    }

    public JSONObject codeword() {
        String code = "{\n" + "		\"177001\": {\n" + "			\"type\": \"Info\"\n" + "		},\n"
                + "		\"177002\": {\n" + "			\"type\": \"Comment\"\n" + "		},\n"
                + "		\"177003\": {\n" + "			\"type\": \"Vote\"\n" + "		},\n" + "		\"177004\": {\n"
                + "			\"status\": true\n" + "		},\n" + "		\"177005\": {\n"
                + "			\"status\": true\n" + "		},\n" + "		\"177006\": {\n"
                + "			\"status\": true\n" + "		},\n" + "		\"177007\": {\n"
                + "			\"status\": true\n" + "		},\n" + "		\"177008\": {\n"
                + "			\"status\": true\n" + "		},\n" + "		\"177009\": {\n"
                + "			\"status\": true\n" + "		},\n" + "		\"177010\": {\n"
                + "			\"status\": true\n" + "		}\n" + "	}";
        return new JSONObject(code);
    }

    public String test() {
        return "test";
    }

    public JSONObject mobile_event() {
        String mo = "{\n" + "		\"Comment\": \"on\",\n" + "		\"Vote\": \"on\",\n" + "		\"Info\": \"on\"\n"
                + "	}";
        return new JSONObject(mo);
    }

    public JSONObject jedis_lockbox(String walletId) {
        JSONObject LockboxDetail;
        if (jedis.get(walletId) == null) {
            LockboxDetail = Service_Lockbox.getEntryById(walletId);
            if (LockboxDetail.getBoolean("status")) {
                jedis.set(walletId, LockboxDetail.toString());
            }
        } else {
            String JsonString = jedis.get(walletId);
            LockboxDetail = new JSONObject(JsonString);
            if (!LockboxDetail.getBoolean("status")) {
                jedis.del(walletId);
                LockboxDetail = Service_Lockbox.getEntryById(walletId);
                if (LockboxDetail.getBoolean("status")) {
                    jedis.set(walletId, LockboxDetail.toString());
                }
            }
        }

        return LockboxDetail;
    }
}
