/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.function;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nonbody
 */
@Service
public class FormatSerch {

    @Autowired
    private Everything ev;

    public String login(String id, String pass) {
        String txt = "{\n" + "    \"query\": {\n" + "                 \"bool\" : {\n"
                + "                    \"must\" : [\n" + "                        { \"term\" : { \"email\" : \"" + id
                + "\" } }, \n" + "                        { \"term\" : { \"password\" : \"" + pass + "\" } } \n"
                + "                    ]\n" + "                }\n" + "            }\n" + "}";

        return txt;
    }

    public String search_id(String id) {
        String txt = "{\n" + "    \"query\": {\n" + "                 \"bool\" : {\n"
                + "                    \"must\" : [\n" + "                        { \"term\" : { \"email\" : \"" + id
                + "\" } }" + "                    ]\n" + "                }\n" + "            }\n" + "}";
        return txt;
    }

    public String gettvprogram() {
        String date_time = ev.format_bydate();
        String date = date_time.split("T")[0];
        String json = "{\n" + "	\"from\": 0,\n" + "    \"size\": 100,\n" + "	\"query\":{\n"
                + "           \"range\" : {\n" + "            \"create_date\" : {\n" + "                \"gte\": \""
                + date + "T00:00:00\",\n" + "                \"lte\": \"" + date + "T23:59:59\",\n"
                + "                \"format\": \"yyyy-MM-dd'T'HH:mm:ss\"\n" + "            }\n" + "        }\n"
                + "        },\n" + "     \"sort\":{ \"create_date\" : {\"order\" : \"asc\"}}  \n" + "}";
        return json;
    }

    public String get_contenttype(String id) {
        String json = "{\"query\":{ \n" + "  \"match\":{\"event_id\":\"" + id + "\"  } \n" + "}\n" + "}";
        return json;
    }

    public String get_ads_home() {
        String json = "{\"query\":{ \n" + "  \"match\":{\"_id\":\"1\"  } \n" + "}\n" + "}";
        return json;
    }

    public String get_tvprogram() {
        String json = "{\n" + "\"query\":{\"match_all\":{}}\n" + "}";
        return json;
    }

    public String get_episode(String id) {
        String json = "{ \"size\":0,\"query\":{\"match\":{\"tvprogram_id\":\"" + id + "\"}}}";
        return json;
    }

    public String get_tvprogram_id(String id) {
        String json = "{ \"query\":{\"match\":{\"tvprogram_id\":\"" + id + "\"}}}";
        return json;
    }

    public String get_event_id(String id) {
        String json = "{ \"query\":{\"match\":{\"tvprogram_id\":\"" + id + "\"}}}";
        return json;
    }

    public String get_eventcontent_id(String id) {
        String json = "{ \"query\":{\"match\":{\"content_id\":\"" + id + "\"}}}";
        return json;
    }

    public String input_info(String url) {
        String json = "{ \"doc\":{\"Info\":[{\"url\":\"" + url + "\"}]}}";
        return json;
    }

    public String input_comment(JSONObject obj) {
        JSONObject doc = new JSONObject();
        doc.put("doc", new JSONObject().put("Comment", new JSONArray().put(obj)));

        String json = doc.toString();
        doc = null;
        return json;
    }

    public String input_vote(JSONArray ary) {
        JSONObject doc = new JSONObject();
        doc.put("doc", new JSONObject().put("Vote", ary));

        String json = doc.toString();
        doc = null;
        return json;
    }

    public String input_ads(JSONArray ary) {
        JSONObject doc = new JSONObject();
        doc.put("doc", new JSONObject().put("Ads", ary));

        String json = doc.toString();
        doc = null;
        return json;
    }
}
