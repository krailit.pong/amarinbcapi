/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.function;

import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nonbody
 */
@Service
public class RestService {

    @Value("${lockbox.auth.key}")
    private String Authorization_lockbox;

    @Value("${header.encryption.key}")
    private String Encryption_lockbox;

    public JSONObject get(String url) {
        JSONObject obj = new JSONObject();
        try {
            HttpResponse<String> response = Unirest.get(url).header("Content-Type", "application/json")
                    .header("X-Encryption-Key", Encryption_lockbox).header("Accept", "application/json")
                    .header("Authorization", "Bearer " + Authorization_lockbox).header("cache-control", "no-cache")
                    .asString();
            obj = new JSONObject(response.getBody());
            obj.put("http_code", response.getStatus());
        } catch (UnirestException ex) {
            Logger.getLogger(RestService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public JSONObject post(String json, String url) {
        JSONObject obj = new JSONObject();
        try {
            System.out.println(json);
            HttpResponse<String> response = Unirest.post(url).header("Content-Type", "application/json")
                    .header("X-Encryption-Key", Encryption_lockbox).header("Accept", "application/json")
                    .header("Authorization", "Bearer " + Authorization_lockbox).header("cache-control", "no-cache")
                    .body(json).asString();
            System.out.println(response.getStatus());
            if (response.getStatus() == 201) {
                obj = new JSONObject();
            } else {
                obj = new JSONObject(response.getBody());
            }
            obj.put("http_code", response.getStatus());
        } catch (UnirestException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(RestService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return obj;
    }

    public JSONObject put(String json, String url) {
        JSONObject obj = new JSONObject();
        try {
            HttpResponse<String> response = Unirest.put(url).header("content-type", "application/json")
                    .header("cache-control", "no-cache").header("postman-token", "f8b66b45-c703-d596-9b04-b9fc5f72eefe")
                    .body(json).asString();
            obj = new JSONObject(response.getBody());
        } catch (UnirestException ex) {
            Logger.getLogger(RestService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return obj;
    }

    public JSONObject del(String url) {
        JSONObject obj = new JSONObject();
        try {
            HttpResponse<String> response = Unirest.delete(url).header("content-type", "application/json")
                    .header("cache-control", "no-cache").header("postman-token", "f8b66b45-c703-d596-9b04-b9fc5f72eefe")
                    .asString();
            obj = new JSONObject(response.getBody());
        } catch (UnirestException ex) {
            Logger.getLogger(RestService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return obj;
    }

}
