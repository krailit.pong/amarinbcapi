/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.function;

import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nonbody
 */
@Service
public class ElasLog {

    @Value("${server.host.elastic}")
    private String server_elastic;

    @Value("${elastic.index.key}")
    private String elastic_index_key;

    public JSONObject post(String dataType,JSONObject json) {
        String url = server_elastic + elastic_index_key + "/" + dataType;
        JSONObject obj = new JSONObject();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = new Date();

        json.put("date_time", dateFormat.format(date));

        try {
            // System.out.println(json);
            HttpResponse<String> response = Unirest.post(url).header("Content-Type", "application/json")
                    .header("Accept", "application/json").header("cache-control", "no-cache").body(json.toString()).asString();
            System.out.println(response.getStatus());
            if (response.getStatus() == 201) {
                obj = new JSONObject();
            } else {
                obj = new JSONObject(response.getBody());
            }
            obj.put("http_code", response.getStatus());
        } catch (UnirestException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(RestService.class.getName()).log(Level.SEVERE, null, ex);
        }
        // System.out.println(obj);
        return obj;
    }

    public JSONObject put(String json, String url) {
        JSONObject obj = new JSONObject();
        try {
            HttpResponse<String> response = Unirest.put(url).header("content-type", "application/json")
                    .header("cache-control", "no-cache").header("postman-token", "f8b66b45-c703-d596-9b04-b9fc5f72eefe")
                    .body(json).asString();
            obj = new JSONObject(response.getBody());
        } catch (UnirestException ex) {
            Logger.getLogger(RestService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return obj;
    }

}
