/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import App.Service.ServiceHorizonInterface;
import App.Service.ServiceLockboxInterface;
import App.Service.ServiceStellarInterfaceBC;
import java.io.IOException;

/**
 *
 * @author Nonbody
 */
@RestController
@RequestMapping("/bc")
public class ApiBcController {

    @Autowired
    private ServiceStellarInterfaceBC Service_StellarBC;

    @RequestMapping(value = "/api/check", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
    @ResponseBody
    String check(@RequestBody String request) throws IOException {
        return "{\"status\": 200}";
    }

    @RequestMapping(value = "/api/CreateActivity", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
    @ResponseBody
    String CreateActivity(@RequestBody String request) throws IOException {
        JSONObject req = new JSONObject(request);
        String activities_id = req.getString("activities_id");
        return Service_StellarBC.generateSingleActivity(activities_id).toString();
    }

    @RequestMapping(value = "/api/CreateChoices", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
    @ResponseBody
    String CreateChoices(@RequestBody String request) throws IOException {
        JSONObject req = new JSONObject(request);
        String activities_id = req.getString("activities_id");
        Integer amount = req.getInt("amountOfChoices");
        if (amount < 1) {
            return "{\"status\": 500,\"message\": \"choices must more than 1\"}";
        }
        return Service_StellarBC.generateMultiChoices(activities_id, amount).toString();
    }

    @RequestMapping(value = "/api/getResultActivity", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
    @ResponseBody
    String getResultActivity(@RequestBody String request) throws IOException {
        JSONObject req = new JSONObject(request);
        String activityId = req.getString("activities_id");
        String amountOfChoices = req.getString("amountOfChoices");

        return Service_StellarBC.resultActivity(activityId, amountOfChoices).toString();
    }

}
