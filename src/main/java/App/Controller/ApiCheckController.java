/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Controller;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.JsonObject;

/**
 *
 * @author Nonbody
 */
@RestController
public class ApiCheckController {

    @RequestMapping("/")
    String check() throws IOException {
        return "200";
    }

    @RequestMapping("/dev")
    String dev(HttpServletRequest request) throws IOException {
        JSONObject obj = new JSONObject();
        obj.put("status", true);
        obj.put("http_code", 200);
        obj.put("request_data", request.getRequestURI());
        obj.put("port", request.getRemotePort());
        obj.put("addr", request.getRemoteAddr());
        obj.put("PathInfo", request.getPathInfo());
        obj.put("Host", request.getRemoteHost());
        obj.put("User", request.getRemoteUser());
        return obj.toString();
        // return "200";
    }
}
