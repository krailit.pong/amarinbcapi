/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import App.Service.ServiceHtmlInterface;
import java.io.IOException;

/**
 *
 * @author Nonbody
 */
@RestController
@RequestMapping("/html")
public class ApiHtmlController {

    @Autowired
    private ServiceHtmlInterface Service_Html;

    @RequestMapping(value = "/api/check", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
    @ResponseBody
    String check(@RequestBody String request) throws IOException {
        return "{\"status\": 200}";
    }

    @RequestMapping(value = "/api/createContent", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
    @ResponseBody
    String createContent(@RequestBody String request) throws IOException {
        JSONObject req = new JSONObject(request);
        String content = req.getString("content");
        // JSONObject result = Service_Html.createContent(content);
        return "";
    }

}
