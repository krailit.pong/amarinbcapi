/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import App.Service.ServiceLockboxInterface;
import App.Service.ServiceLogInterface;
import App.Service.ServiceStellarInterface;
import App.Service.ServiceStellarInterfaceBC;
import App.function.ElasLog;
import App.function.Everything;
import redis.clients.jedis.JedisCluster;

import java.io.IOException;

/**
 *
 * @author Nonbody
 */
@RestController
@RequestMapping("/mobile")
public class ApiMobileController {

    @Autowired
    private ServiceLockboxInterface Service_Lockbox;

    @Autowired
    private ServiceStellarInterface Service_Stellar;

    @Autowired
    private ServiceStellarInterfaceBC Service_StellarBC;

    @Autowired
    private ServiceLogInterface serviceLog;

    @Autowired
    private JedisCluster jedis;

    @Autowired
    private ElasLog elaslog;

    // private static Everything everything = new Everything();

    @Autowired
    private Everything Everything;

    @Value("${point.name}")
    private String Point_name;

    @Value("${amarin.holder.private_key}")
    private String amarin_holder_private_key;

    @Value("${amarin.dis.private_key}")
    private String amarin_dis_private_key;

    @RequestMapping(value = "/api/check", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
    @ResponseBody
    String check(@RequestBody String request) throws IOException {
        JSONObject obj = new JSONObject();
        obj.put("status", true);
        obj.put("http_cde", 200);
        return obj.toString();
    }

    @RequestMapping(value = "/api/createWalletById", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
    @ResponseBody
    String createWalletById(@RequestBody String request) {
        JSONObject obj = new JSONObject(request);
        String walletId = obj.getString("_id");

        JSONObject data_all = Service_Stellar.createBoxAndWallet(walletId);
        Service_Stellar.taskCreate(walletId);
        serviceLog.setLogCommon("", "createUser", walletId, "", "", "", data_all.getString("message"),
                data_all.getBoolean("status"));
        // need to save result to database
        return data_all.toString();
    }

    @RequestMapping(value = "/api/getWalletBalance", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
    @ResponseBody
    String getWalletBalance(@RequestBody String request) {
        JSONObject req = new JSONObject(request);
        String walletId = req.getString("_id");

        JSONObject data_all = Service_Stellar.getWalletDetail(walletId);
        serviceLog.setLogCommon("", "getWallet", walletId, "", "", "", data_all.getString("message"),
                data_all.getBoolean("status"));

        return data_all.toString();
    }

    @RequestMapping(value = "/api/topUpPoint", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
    @ResponseBody
    public String topUpPoint(@RequestBody String request) throws IOException {
        JSONObject req = new JSONObject(request);
        String walletId = req.getString("_id");
        Integer amount = req.getInt("amount");
        if (amount == 0) {
            JSONObject er = new JSONObject();
            er.put("status", false);
            er.put("message", "required amount more than 0");
            return er.toString();
        }

        JSONObject LockboxDetail = Everything.jedis_lockbox(walletId);
        JSONObject data_all = new JSONObject();
        String message = "";
        if (LockboxDetail.getBoolean("status")) {
            JSONObject data_object = LockboxDetail.getJSONObject("data_object");
            // need to recheck
            String privateReciveKey = data_object.getString("privateKey");

            // data_all = Service_Stellar.giveCoinAsync(amarin_holder_private_key,
            // amarin_dis_private_key, privateReciveKey,
            // Point_name, amount.toString());
            // message = data_all.getString("message");


            data_all = Service_Stellar.getWalletDetail(walletId);
            if (data_all.getString("message").equals("success")) {
                Double newBalance = Double.parseDouble(data_all.getJSONObject("data_object").getString("balance"))
                        + Double.parseDouble(amount.toString());
                data_all.getJSONObject("data_object").put("balance", newBalance);
            }

            // Do Async Function
            Service_Stellar.giveCoinAsync(amarin_holder_private_key, amarin_dis_private_key, privateReciveKey,
                    Point_name, amount.toString());
        } else {
            data_all.put("message", LockboxDetail.getString("message"));
            data_all.put("status", false);
            data_all.put("_id", walletId);
            message = LockboxDetail.getString("message");
        }
        serviceLog.setLogCommon(Point_name, "topUpPoint", walletId, "Amarin_System", walletId, amount.toString(),
                message, data_all.getBoolean("status"));

        return data_all.toString();
    }

    @RequestMapping(value = "/api/reducePoint", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
    @ResponseBody
    String reducePoint(@RequestBody String request) throws IOException {
        JSONObject req = new JSONObject(request);
        String walletId = req.getString("_id");
        Integer amount = req.getInt("amount");
        if (amount == 0) {
            JSONObject er = new JSONObject();
            er.put("status", false);
            er.put("message", "required amount more than 0");
            return er.toString();
        }

        JSONObject LockboxDetail = Everything.jedis_lockbox(walletId);
        JSONObject data_all = new JSONObject();
        String message = "";
        if (LockboxDetail.getBoolean("status")) {
            JSONObject data_object = LockboxDetail.getJSONObject("data_object");
            // need to recheck
            String privateSourceKey = data_object.getString("privateKey");

            /* Sync WAY */
            // data_all = Service_Stellar.giveCoin(amarin_holder_private_key, privateSourceKey, amarin_dis_private_key,
            //         Point_name, amount.toString());
            // message = data_all.getString("message");

            data_all = Service_Stellar.getWalletDetail(walletId);
            if (data_all.getString("message").equals("success")) {
                Double newBalance = Double.parseDouble(data_all.getJSONObject("data_object").getString("balance"))
                        - Double.parseDouble(amount.toString());
                data_all.getJSONObject("data_object").put("balance", newBalance);
            }
            message = data_all.getString("message");

            // Do Async Function
            Service_Stellar.giveCoinAsync(amarin_holder_private_key, privateSourceKey, amarin_dis_private_key,
                    Point_name, amount.toString());
        } else {
            data_all.put("message", LockboxDetail.getString("message"));
            data_all.put("status", false);
            data_all.put("_id", walletId);
            message = LockboxDetail.getString("message");
        }

        serviceLog.setLogCommon(Point_name, "reducePoint", walletId, walletId, "Amarin_System", amount.toString(),
                message, data_all.getBoolean("status"));

        return data_all.toString();
    }

    @RequestMapping(value = "/api/voteChoices", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
    @ResponseBody
    String VoteChoices(@RequestBody String request) throws IOException {
        JSONObject req = new JSONObject(request);
        JSONObject data_all = new JSONObject();
        String activityId = req.getString("activity_id");
        String walletId = req.getString("_id");
        String Choices_id = req.getString("line_no");

        data_all = Service_StellarBC.giveChoicesPoint(walletId, activityId, Choices_id);
        return data_all.toString();
    }

}
