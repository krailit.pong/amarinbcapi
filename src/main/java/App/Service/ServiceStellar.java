package App.Service;

import App.function.ElasLog;
import App.function.Everything;
import App.function.FormatSerch;
import App.function.RestService;
import redis.clients.jedis.JedisCluster;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import org.stellar.sdk.Asset;
import org.stellar.sdk.AssetTypeCreditAlphaNum12;
import org.stellar.sdk.AssetTypeNative;
import org.stellar.sdk.ChangeTrustOperation;
import org.stellar.sdk.CreateAccountOperation;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.Memo;
import org.stellar.sdk.Network;
import org.stellar.sdk.PaymentOperation;

import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.*;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.google.common.base.Strings;

import org.stellar.sdk.Server;
import org.stellar.sdk.Transaction;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

@Service
public class ServiceStellar implements ServiceStellarInterface {

    @Autowired
    private RestService rest;

    @Autowired
    private ServiceLockboxInterface Service_Lockbox;

    @Autowired
    private ElasLog elaslog;

    @Autowired
    private JedisCluster jedis;

    @Autowired
    private Everything Everything;

    @Value("${server.host.stellar}")
    private String ServerUrl;

    @Value("${point.name}")
    private String Point_name;

    @Value("${root.acc.stellar.private_key}")
    private String root_acc_stellar_private_key;

    @Value("${amarin.holder.private_key}")
    private String amarin_holder_private_key;

    @Value("${amarin.dis.private_key}")
    private String amarin_dis_private_key;

    @Value("${amarin.dis.public_key}")
    private String amarin_dis_public_key;

    @Override
    public JSONObject createBoxAndWallet(String walletId) {
        JSONObject data_all = new JSONObject();
        data_all.put("status", false);
        JSONObject resultBoxes = Service_Lockbox.setBoxesById(walletId);
        if (resultBoxes.getBoolean("status")) {
            data_all.put("status", true);
            data_all.put("message", "success");
        } else {
            data_all.put("message", resultBoxes.getString("message"));
        }

        return data_all;
    }

    @Override
    @Async
    public JSONObject taskCreate(String walletId) {
        JSONObject data_all = new JSONObject();
        data_all.put("status", false);
        JSONObject resultWallet = createWallet();
        if (resultWallet.getBoolean("status")) {
            JSONObject resultEntry = Service_Lockbox.setEntryById(resultWallet, walletId);
            if (resultEntry.getBoolean("status")) {
                data_all.put("status", true);
                data_all.put("message", resultEntry.getString("message"));
            } else {
                data_all.put("message", resultEntry.getString("message"));
            }
        } else {
            data_all.put("message", resultWallet.getString("message"));
        }
        return data_all;
    }

    @Override
    public JSONObject createWallet() {
        JSONObject data_all = new JSONObject();
        data_all.put("status", false);
        data_all.put("publicKey", "");
        data_all.put("privateKey", "");

        KeyPair pair = KeyPair.random();
        String privateKey = new String(pair.getSecretSeed());
        String publicKey = pair.getAccountId();

        try {
            JSONObject result = addToFriendBot(publicKey);
            if (!result.getBoolean("status")) {
                data_all.put("message", "By function addToFriendBot " + result.getString("message"));
            } else {
                System.out.println("addToFriendBot Success");
            }
        } catch (Exception e) {
            data_all.put("message", "Try Catch in addToFriendBot " + e.getMessage());
            return data_all;
        }

        try {
            JSONObject trustAssetresult = trustAsset(amarin_holder_private_key, privateKey, Point_name, "100000");
            if (!trustAssetresult.getBoolean("status")) {
                data_all.put("message", "By function setPoint " + trustAssetresult.getString("message"));
                System.out.println(trustAssetresult);
            } else {
                System.out.println("trustAsset Success");
            }
        } catch (Exception e) {
            data_all.put("message", "Try Catch in trustAsset " + e.getMessage());
            return data_all;
        }

        try {
            // init starter point
            JSONObject giveCointresult = giveCoin(amarin_holder_private_key, amarin_dis_private_key, privateKey,
                    Point_name, "1");
            if (giveCointresult.getBoolean("status")) {
                data_all.put("status", true);
                data_all.put("message", "success");
                data_all.put("publicKey", publicKey);
                data_all.put("privateKey", privateKey);
                System.out.println("giveCoin Success");
            } else {
                data_all.put("message", "By function in giveCoin " + giveCointresult.getString("message"));
            }
        } catch (Exception e) {
            data_all.put("message", "Try Catch in giveCoin " + e.getMessage());
            return data_all;
        }

        return data_all;
    }

    @Override
    @Async
    public JSONObject addToFriendBot(String publicKey) throws MalformedURLException, IOException {
        Network network = new Network("Betimes SDF Network ; Dec 2018");
        Network.use(network);
        Server server = new Server(ServerUrl);

        // Root Account Stellar Amarin
        KeyPair source = KeyPair.fromSecretSeed(root_acc_stellar_private_key);
        KeyPair destination = KeyPair.fromAccountId(publicKey);

        AccountResponse sourceAccount = server.accounts().account(source);
        Transaction tranCreateAccount = new Transaction.Builder(sourceAccount)
                .addOperation(new CreateAccountOperation.Builder(destination, "10000").setSourceAccount(source).build())
                .addMemo(Memo.text("Create Account")).build();
        tranCreateAccount.sign(source);

        SubmitTransactionResponse response = server.submitTransaction(tranCreateAccount);
        JSONObject obj = new JSONObject();
        if (!response.isSuccess()) {
            obj.put("message", response.getExtras().getResultCodes().getTransactionResultCode());
            obj.put("operationResult", response.getExtras().getResultCodes().getOperationsResultCodes());
            obj.put("getEnvelopeXdr", response.getExtras().getEnvelopeXdr());
            obj.put("status", false);
        } else {
            obj.put("message", "success");
            obj.put("status", true);
        }

        // System.out.println(obj);
        return obj;
    }

    @Override
    @Async
    public JSONObject trustAsset(String privateIssuerKey, String privateReciveKey, String AssetName,
            String AmountHolder) throws IOException {
        Network network = new Network("Betimes SDF Network ; Dec 2018");
        Network.use(network);
        Server server = new Server(ServerUrl);

        // Keys for accounts to issue and receive the new asset
        KeyPair issuingKeys = KeyPair.fromSecretSeed(privateIssuerKey);
        // KeyPair.fromSecretSeed(privateCreateKey);
        KeyPair receivingKeys = KeyPair.fromSecretSeed(privateReciveKey);

        // Create an object to represent the new asset
        // Asset assets = Asset.createNonNativeAsset(AssetName, issuingKeys);
        Asset assets = new AssetTypeCreditAlphaNum12(AssetName, issuingKeys);

        // First, the receiving account must trust the asset
        AccountResponse receiving = server.accounts().account(receivingKeys);
        Transaction allowAssets = new Transaction.Builder(receiving).addOperation(
                // The `ChangeTrust` operation creates (or alters) a trustline
                // The second parameter limits the amount the account can hold
                new ChangeTrustOperation.Builder(assets, AmountHolder).build()).build();
        allowAssets.sign(receivingKeys);

        JSONObject obj = new JSONObject();
        SubmitTransactionResponse response = server.submitTransaction(allowAssets);
        if (!response.isSuccess()) {
            obj.put("message", response.getExtras().getResultCodes().getTransactionResultCode());
            obj.put("operationResult", response.getExtras().getResultCodes().getOperationsResultCodes());
            obj.put("getEnvelopeXdr", response.getExtras().getEnvelopeXdr());
            obj.put("status", false);
        } else {
            obj.put("message", "success");
            obj.put("status", true);
        }
        return obj;
    }

    @Override
    public JSONObject giveCoin(String privateIssuerKey, String privateSenderKey, String privateReciveKey,
            String AssetName, String amount) throws IOException {
        JSONObject obj = new JSONObject();

        JSONObject checkBalance = checkBalance(privateSenderKey, AssetName);
        if (checkBalance.getBoolean("status")) {
            Double balance = Double.parseDouble(checkBalance.getString("balance"));
            Double amount_db = Double.parseDouble(amount);
            if (balance >= amount_db) {
                Network network = new Network("Betimes SDF Network ; Dec 2018");
                Network.use(network);
                Server server = new Server(ServerUrl);

                // Owner Point
                KeyPair source = KeyPair.fromSecretSeed(privateIssuerKey);
                // Sender Point
                KeyPair sender = KeyPair.fromSecretSeed(privateSenderKey);
                // Destination Point
                KeyPair destination = KeyPair.fromSecretSeed(privateReciveKey);

                AccountResponse senderAccount = server.accounts().account(sender);
                Transaction giveCoinTx = new Transaction.Builder(senderAccount)
                        .addOperation(new PaymentOperation.Builder(destination,
                                new AssetTypeCreditAlphaNum12(AssetName, source), amount).build())
                        .build();
                giveCoinTx.sign(sender);

                SubmitTransactionResponse response = server.submitTransaction(giveCoinTx);
                if (!response.isSuccess()) {
                    obj.put("message", response.getExtras().getResultCodes().getTransactionResultCode());
                    obj.put("operationResult", response.getExtras().getResultCodes().getOperationsResultCodes());
                    obj.put("getEnvelopeXdr", response.getExtras().getEnvelopeXdr());
                    obj.put("status", false);
                } else {
                    obj.put("message", "success");
                    obj.put("status", true);
                }
            } else {
                obj.put("message", "balance not enough to reduce !");
                obj.put("status", false);
            }
        } else {
            obj.put("message", " fail to get balance or this account not have asset !");
            obj.put("status", false);
        }

        return obj;
    }

    @Override
    @Async
    public JSONObject giveCoinAsync(String privateIssuerKey, String privateSenderKey, String privateReciveKey,
            String AssetName, String amount) throws IOException {
        JSONObject obj = new JSONObject();
        /* JSONObject checkBalance = checkBalance(privateSenderKey, AssetName);
        if (checkBalance.getBoolean("status")) {
            Double balance = Double.parseDouble(checkBalance.getString("balance"));
            Double amount_db = Double.parseDouble(amount);
            if (balance >= amount_db) {
                Network network = new Network("Betimes SDF Network ; Dec 2018");
                Network.use(network);
                Server server = new Server(ServerUrl);
        
                // Owner Point
                KeyPair source = KeyPair.fromSecretSeed(privateIssuerKey);
                // Sender Point
                KeyPair sender = KeyPair.fromSecretSeed(privateSenderKey);
                // Destination Point
                KeyPair destination = KeyPair.fromSecretSeed(privateReciveKey);
        
                AccountResponse senderAccount = server.accounts().account(sender);
                Transaction giveCoinTx = new Transaction.Builder(senderAccount)
                        .addOperation(new PaymentOperation.Builder(destination,
                                new AssetTypeCreditAlphaNum12(AssetName, source), amount).build())
                        .build();
                giveCoinTx.sign(sender);
        
                SubmitTransactionResponse response = server.submitTransaction(giveCoinTx);
                if (!response.isSuccess()) {
                    obj.put("message", response.getExtras().getResultCodes().getTransactionResultCode());
                    obj.put("operationResult", response.getExtras().getResultCodes().getOperationsResultCodes());
                    obj.put("getEnvelopeXdr", response.getExtras().getEnvelopeXdr());
                    obj.put("status", false);
                } else {
                    obj.put("message", "success");
                    obj.put("status", true);
                }
            } else {
                obj.put("message", "balance not enough to reduce !");
                obj.put("status", false);
            }
        } else {
            obj.put("message", " fail to get balance or this account not have asset !");
            obj.put("status", false);
        } */
        obj = this.giveCoin(privateIssuerKey, privateSenderKey, privateReciveKey, AssetName, amount);
        return obj;
    }

    @Override
    public JSONObject getWalletDetail(String walletId) {
        JSONObject data_all = new JSONObject();
        data_all.put("status", false);
        JSONObject LockboxDetail = Everything.jedis_lockbox(walletId);
        if (LockboxDetail.getBoolean("status")) {
            JSONObject data_object = LockboxDetail.getJSONObject("data_object");
            String publicKey = data_object.getString("publicKey");
            String url = String.format(ServerUrl + "/accounts/" + publicKey);
            JSONObject call = rest.get(url);
            if (call.has("balances") && !call.isNull("balances")) {
                data_all.put("status", true);
                data_all.put("message", "success");
                for (int i = 0; i < call.getJSONArray("balances").length(); i++) {
                    JSONObject asset = call.getJSONArray("balances").getJSONObject(i);
                    if (!asset.getString("asset_type").equals("native")) {
                        if (asset.getString("asset_code").equals(Point_name)) {
                            data_all.put("data_object", asset);
                        }
                    }
                }
            } else {
                data_all.put("message", call);
            }
        } else {
            data_all.put("message", LockboxDetail.getString("message"));
        }

        return data_all;
    }

    @Override
    public JSONObject checkBalance(String privateKey, String AssetName) throws IOException {
        Network network = new Network("Betimes SDF Network ; Dec 2018");
        Network.use(network);
        Server server = new Server(ServerUrl);
        KeyPair pair = KeyPair.fromSecretSeed(privateKey);

        AccountResponse account = server.accounts().account(pair);
        JSONObject response = new JSONObject();
        response.put("status", false);
        for (AccountResponse.Balance balance : account.getBalances()) {
            if (AssetName.equals(balance.getAssetCode())) {
                response.put("status", true);
                response.put("balance", balance.getBalance());

                return response;
            }
        }

        return response;
    }

}