/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Service;

import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONObject;

/**
 *
 * @author Nonbody
 */
public interface ServiceStellarInterface {

        JSONObject giveCoin(String privateIssuerKey, String privateSenderKey, String privateReciveKey, String AssetName,
                        String amount) throws IOException;
                        
        JSONObject giveCoinAsync(String privateIssuerKey, String privateSenderKey, String privateReciveKey, String AssetName,
                        String amount) throws IOException;

        JSONObject createBoxAndWallet(String walletId);

        JSONObject taskCreate(String walletId);

        JSONObject trustAsset(String privateIssuerKey, String privateReciveKey, String AssetName, String AmountHolder)
                        throws IOException;

        JSONObject createWallet();

        JSONObject getWalletDetail(String publicKey);

        JSONObject addToFriendBot(String publicKey) throws MalformedURLException, IOException;

        JSONObject checkBalance(String privateKey, String AssetName) throws IOException;

}
