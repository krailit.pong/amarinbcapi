package App.Service;

import App.function.Everything;
import App.function.FormatSerch;
import App.function.RestService;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import org.stellar.sdk.Asset;
import org.stellar.sdk.AssetTypeNative;
import org.stellar.sdk.ChangeTrustOperation;
import org.stellar.sdk.CreateAccountOperation;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.Memo;
import org.stellar.sdk.Network;
import org.stellar.sdk.PaymentOperation;

import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.io.*;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.google.common.base.Strings;

import org.stellar.sdk.Server;
import org.stellar.sdk.Transaction;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;
import org.stellar.sdk.xdr.CreateAccountResult;

public class Test {

    public static void main(String[] args) throws IOException {
        // KeyPair pair = KeyPair.random();
        // String getSecretSeed = new String(pair.getSecretSeed());
        // String getAccountId = pair.getAccountId();

        // System.out.println(getSecretSeed);
        // System.out.println(getAccountId);

        // Network network = new Network("Betimes SDF Network ; Dec 2018");
        // Network.use(network);
        // Server server = new Server("http://10.10.15.104:8000");

        // // Root Account Stellar Amarin
        // KeyPair source =
        // KeyPair.fromSecretSeed("SBTJR4A6BHMG6W4DDN4ZPCK4BJHZTPRQ2P2SU54B2BA2E53LSEPE4D5C");
        // KeyPair destination = KeyPair.fromAccountId(getAccountId);

        // AccountResponse sourceAccount = server.accounts().account(source);
        // Transaction tranCreateAccount = new Transaction.Builder(sourceAccount)
        // .addOperation(new CreateAccountOperation.Builder(destination,
        // "1000000").setSourceAccount(source).build())
        // .addMemo(Memo.text("Create Account")).build();
        // tranCreateAccount.sign(source);

        // SubmitTransactionResponse response =
        // server.submitTransaction(tranCreateAccount);
        // JSONObject obj = new JSONObject();
        // if (!response.isSuccess()) {
        // obj.put("message",
        // response.getExtras().getResultCodes().getTransactionResultCode());
        // obj.put("operationResult",
        // response.getExtras().getResultCodes().getOperationsResultCodes());
        // obj.put("status", false);
        // } else {
        // obj.put("message", "done");
        // obj.put("status", true);
        // }
        // System.out.println(obj);

        // Keys for accounts to issue and receive the new asset
        // KeyPair issuingKeys =
        // KeyPair.fromSecretSeed("SCNGRGA545L7K4KDALNKL6LXNJ5ZN5F42AWTBMK7QKV32AY6XZXLG6S6");
        // KeyPair receivingKeys =
        // KeyPair.fromSecretSeed("SCJ3LBN64QECO6J3UT3ASUKTKC6SJDUQEFKBBTJ4Q4QHLP5UPZCDGWKX");

        // // Create an object to represent the new asset
        // Asset astroDollar = Asset.createNonNativeAsset("AmarinCoins", issuingKeys);

        // // First, the receiving account must trust the asset
        // AccountResponse receiving = server.accounts().account(receivingKeys);
        // Transaction allowAstroDollars = new
        // Transaction.Builder(receiving).addOperation(
        // // The `ChangeTrust` operation creates (or alters) a trustline
        // // The second parameter limits the amount the account can hold
        // new ChangeTrustOperation.Builder(astroDollar, "100000000").build()).build();
        // allowAstroDollars.sign(receivingKeys);
        // server.submitTransaction(allowAstroDollars);

        // // Second, the issuing account actually sends a payment using the asset
        // AccountResponse issuing = server.accounts().account(issuingKeys);
        // Transaction sendAstroDollars = new Transaction.Builder(issuing)
        // .addOperation(new PaymentOperation.Builder(receivingKeys, astroDollar,
        // "100000000").build()).build();
        // sendAstroDollars.sign(issuingKeys);

        // SubmitTransactionResponse response =
        // server.submitTransaction(sendAstroDollars);
        // JSONObject obj = new JSONObject();
        // if (!response.isSuccess()) {
        // obj.put("message",
        // response.getExtras().getResultCodes().getTransactionResultCode());
        // obj.put("operationResult",
        // response.getExtras().getResultCodes().getOperationsResultCodes());
        // obj.put("status", false);
        // } else {
        // obj.put("message", "done");
        // obj.put("status", true);
        // }
        // System.out.println((double) 5);
        // System.out.println(Double.parseDouble("5.60"));
        // String Ac = "133Ep";
        // System.out.println(Strings.padStart(Ac, 12, '0'));
        // System.out.println(Strings.padStart(Ac, 12, '0').length());
        // DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        // Date date = new Date();
        // System.out.println(dateFormat.format(date)); //2018-12-26 15:26:53

        Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();
        // Jedis Cluster will attempt to discover cluster nodes automatically
        jedisClusterNodes.add(new HostAndPort("10.10.14.74", 7000));
        jedisClusterNodes.add(new HostAndPort("10.10.14.75", 7000));
        jedisClusterNodes.add(new HostAndPort("10.10.14.76", 7000));
        JedisCluster jedis = new JedisCluster(jedisClusterNodes);

        JSONObject json = new JSONObject();
        json.put("a", "b");
        jedis.set("User:1", json.toString());
        jedis.close();
    }
}