package App.Service;

import App.function.Everything;
import App.function.FormatSerch;
import App.function.RestService;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import org.stellar.sdk.Asset;
import org.stellar.sdk.AssetTypeNative;
import org.stellar.sdk.ChangeTrustOperation;
import org.stellar.sdk.CreateAccountOperation;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.Memo;
import org.stellar.sdk.Network;
import org.stellar.sdk.PaymentOperation;

import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.*;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.stellar.sdk.Server;
import org.stellar.sdk.Transaction;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

@Service
public class ServiceHtml implements ServiceHtmlInterface {

    @Autowired
    private RestService rest;

    @Override
    public JSONObject createContent(String Content) throws IOException {
        byte[] content_decode = Base64.getDecoder().decode(Content);
        String main_content = new String(content_decode);

        JSONObject result = this.htmlFile(main_content);
        return result;
    }


    public JSONObject htmlFile(String Content) throws IOException {
        String Style = new String(Files.readAllBytes(Paths.get("html_code/css/main.css")));

        String iframeStart = "<div class='resp-container'><iframe class='resp-iframe'";
        String iframeStop = "</iframe></div>";
        Content = Content.replace("<iframe", iframeStart);
        Content = Content.replace("</iframe>", iframeStop);

        String html = "<!DOCTYPE html><html lang='en'>";
        html += "<head><title></title>";
        html += "<meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1'>";
        html += "</head>";
        html += "<style>";
        html += Style.toString();
        html += "</style>";
        html += "<body>";
        html += "<div class='main'>";
        html += Content;
        html += "</div>";
        html += "</body>";
        html += "</html>";

        JSONObject obj = new JSONObject();
        try {
            FileUtils.writeStringToFile(new File("html_code/test.html"), html);
            obj.put("status", true);
            obj.put("message", "");
        } catch (Exception e) {
            obj.put("status", true);
            obj.put("message", e.getMessage());
        }
        return obj;
    }

}