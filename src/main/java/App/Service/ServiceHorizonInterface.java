/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Service;

import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONObject;

/**
 *
 * @author Nonbody
 */
public interface ServiceHorizonInterface {
    JSONObject createWallet();
    JSONObject getWalletDetail(String publicKey);
    JSONObject addToFriendBot(String publicKey) throws MalformedURLException, IOException;
}
