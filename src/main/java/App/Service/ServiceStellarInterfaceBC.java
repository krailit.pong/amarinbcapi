/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Service;

import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Nonbody
 */
public interface ServiceStellarInterfaceBC {

        JSONObject createAssetType(String issuerPrivateKey, String distributorPrivateKey, String AssetName)
                        throws IOException;

        // activity
        JSONObject generateSingleActivity(String activityId) throws IOException;

        JSONObject generateMultiChoices(String activityId, Integer amount) throws IOException;

        JSONObject createBoxAndWalletForActivity(String walletId) throws IOException;
        // activity

        JSONObject trustAsset(String privateIssuerKey, String privateReciveKey, String AssetName, String AmountHolder)
                        throws IOException;

        JSONObject addToFriendBot(String publicKey) throws MalformedURLException, IOException;

        JSONObject giveChoicesPoint(String walletId,String activityId,String Choices_id) throws IOException;

        JSONObject giveCoin(String privateIssuerKey, String privateSenderKey, String privateReciveKey, String AssetName)
                        throws IOException;

        JSONObject createWallet();

        JSONObject resultActivity(String activityId,String amountOfChoices) throws IOException;
}
