/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Service;

import App.model.input.UpdateProfileModel;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Nonbody
 */
public interface ServiceLockboxInterface {
    JSONObject getEntryById(String walletId);
    JSONObject setBoxesById(String walletId);
    JSONObject setEntryById(JSONObject resultWallet,String walletId);
}
