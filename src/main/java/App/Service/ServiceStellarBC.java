package App.Service;

import App.function.Everything;
import App.function.FormatSerch;
import App.function.RestService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import org.stellar.sdk.Asset;
import org.stellar.sdk.AssetTypeCreditAlphaNum12;
import org.stellar.sdk.AssetTypeCreditAlphaNum4;
import org.stellar.sdk.AssetTypeNative;
import org.stellar.sdk.ChangeTrustOperation;
import org.stellar.sdk.CreateAccountOperation;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.Memo;
import org.stellar.sdk.Network;
import org.stellar.sdk.PaymentOperation;

import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import com.google.common.base.Strings;

import org.stellar.sdk.Server;
import org.stellar.sdk.Transaction;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

@Service
public class ServiceStellarBC implements ServiceStellarInterfaceBC {

    @Autowired
    private RestService rest;

    @Autowired
    private ServiceLockbox Service_Lockbox;

    @Autowired
    private ServiceHorizon Service_Horizon;

    @Autowired
    private ServiceStellarInterface Service_Stellar;

    @Autowired
    private ServiceLogInterface serviceLog;

    // private static Everything everything = new Everything();

    @Autowired
    private Everything Everything;
    // @Autowired
    // private JedisCluster jedis;

    @Value("${server.host.stellar}")
    private String ServerUrl;

    @Value("${point.name}")
    private String Point_name;

    @Value("${root.acc.stellar.private_key}")
    private String root_acc_stellar_private_key;

    @Value("${amarin.holder.private_key}")
    private String amarin_holder_private_key;

    @Value("${amarin.dis.private_key}")
    private String amarin_dis_private_key;

    @Value("${amarin.dis.public_key}")
    private String amarin_dis_public_key;

    // private static KeyPair issuerKeyPair = KeyPair
    // .fromSecretSeed("SBZTDQTRKZ7YYINI3IFSSGYQQ6VFMZ2NIYXOOGLGOAYQ45IOXEM6RQYX");

    private Network getNetwork() {
        return new Network("Betimes SDF Network ; Dec 2018");
    }

    @Async
    @Override
    public JSONObject generateSingleActivity(String activityId) throws IOException {
        String AssetName = Everything.renameEp(activityId);
        JSONObject obj = new JSONObject();
        System.out.println("Gen Episode AssetName = " + AssetName);
        JSONObject issuerObject = createBoxAndWalletForActivity(activityId + "Issuer");
        if (issuerObject.getBoolean("status")) {

            String IssuerPrivateKey = issuerObject.getJSONObject("data").getString("privateKey");
            // String IssuerPublicKey =
            // issuerObject.getJSONObject("data").getString("publicKey");

            JSONObject distributorObject = createBoxAndWalletForActivity(activityId + "Distributor");
            if (distributorObject.getBoolean("status")) {

                String DistributorPrivateKey = distributorObject.getJSONObject("data").getString("privateKey");
                // String DistributorPublicKey =
                // distributorObject.getJSONObject("data").getString("publicKey");
                // System.out.println(DistributorPrivateKey);
                // System.out.println(DistributorPublicKey);

                /*
                 * 1. Create Asset 2. Make trust for distributor 3. Make trust for All Choices.
                 */

                JSONObject createAssetType = createAssetType(IssuerPrivateKey, DistributorPrivateKey, AssetName);
                if (createAssetType.getBoolean("status")) {
                    obj.put("status", true);
                    obj.put("message", "success");
                } else {
                    obj.put("status", false);
                    obj.put("message", "createAssetType " + createAssetType.getString("message"));
                }
            } else {
                obj.put("status", false);
                obj.put("message", "distributorObject " + distributorObject.getString("message"));
            }
        } else {
            obj.put("status", false);
            obj.put("message", "issuerObject " + issuerObject.getString("message"));
        }

        return obj;
    }

    @Override
    public JSONObject createAssetType(String issuerPrivateKey, String distributorPrivateKey, String AssetName)
            throws IOException {
        Network network = new Network("Betimes SDF Network ; Dec 2018");
        Network.use(network);
        Server server = new Server(ServerUrl);

        // Keys for accounts to issue and receive the new asset
        KeyPair issuingKeys = KeyPair.fromSecretSeed(issuerPrivateKey);
        KeyPair distributeKeys = KeyPair.fromSecretSeed(distributorPrivateKey);

        // Create an object to represent the new asset
        Asset assets = Asset.createNonNativeAsset(AssetName, issuingKeys);

        // First, the receiving account must trust the asset
        AccountResponse receiving = server.accounts().account(distributeKeys);
        Transaction allowAssets = new Transaction.Builder(receiving).addOperation(
                // The `ChangeTrust` operation creates (or alters) a trustline
                // The second parameter limits the amount the account can hold
                new ChangeTrustOperation.Builder(assets, "100000000").build()).build();
        allowAssets.sign(distributeKeys);
        server.submitTransaction(allowAssets);

        // Second, the issuing account actually sends a payment using the asset
        AccountResponse issuing = server.accounts().account(issuingKeys);
        Transaction sendAssets = new Transaction.Builder(issuing)
                .addOperation(new PaymentOperation.Builder(distributeKeys, assets, "100000000").build()).build();
        sendAssets.sign(issuingKeys);

        JSONObject obj = new JSONObject();
        SubmitTransactionResponse response = server.submitTransaction(sendAssets);
        if (!response.isSuccess()) {
            obj.put("message", response.getExtras().getResultCodes().getTransactionResultCode());
            obj.put("operationResult", response.getExtras().getResultCodes().getOperationsResultCodes());
            obj.put("getEnvelopeXdr", response.getExtras().getEnvelopeXdr());
            obj.put("status", false);
        } else {
            obj.put("message", "done");
            obj.put("status", true);
        }
        return obj;
    }

    @Async
    @Override
    public JSONObject generateMultiChoices(String activityId, Integer amount) throws IOException {
        String AssetName = Everything.renameEp(activityId);
        JSONObject obj = new JSONObject();
        obj.put("status", false);

        // ********* Loop Create Choice Wallet follow amount. *********

        String IssuerPrivateKey = "";
        JSONObject entry = Everything.jedis_lockbox(activityId + "Issuer");
        if (entry.getBoolean("status")) {
            JSONObject data_object = entry.getJSONObject("data_object");
            IssuerPrivateKey = data_object.getString("privateKey");
        } else {
            obj.put("message", "Issuer Not Found !");
            return obj;
        }

        try {
            for (Integer i = 1; i <= amount; i++) {
                JSONObject choiceObject = createBoxAndWalletForActivity(activityId + "Choice" + i);
                if (choiceObject.getBoolean("status")) {
                    JSONObject data = choiceObject.getJSONObject("data");
                    System.out.println(data);
                    JSONObject result = trustAsset(IssuerPrivateKey, data.getString("privateKey"), AssetName,
                            "100000000");
                    if (!result.getBoolean("status")) {
                        obj.put("message", result.getString("message"));
                        return obj;
                    }
                    Thread.sleep(1000);
                } else {
                    obj.put("message", choiceObject.getString("message"));
                    return obj;
                }

            }
        } catch (InterruptedException ex) {
            obj.put("message", ex.getMessage());
            return obj;
        }

        obj.put("status", true);
        obj.put("message", "success");

        return obj;
    }

    @Override
    public JSONObject createBoxAndWalletForActivity(String walletId) {
        JSONObject data_all = new JSONObject();
        data_all.put("status", false);
        JSONObject resultBoxes = Service_Lockbox.setBoxesById(walletId);
        if (resultBoxes.getBoolean("status")) {
            JSONObject resultWallet = createWallet();
            if (resultWallet.getBoolean("status")) {
                JSONObject resultEntry = Service_Lockbox.setEntryById(resultWallet, walletId);
                if (resultEntry.getBoolean("status")) {
                    data_all.put("status", true);
                    data_all.put("message", "Success");
                    data_all.put("data", resultWallet);
                } else {
                    data_all.put("message", resultEntry.getString("message"));
                }
            } else {
                data_all.put("message", resultWallet.getString("message"));
            }
        } else {
            data_all.put("message", resultBoxes.getString("message"));
        }
        return data_all;
    }

    @Override
    public JSONObject createWallet() {
        JSONObject data_all = new JSONObject();
        data_all.put("status", false);
        data_all.put("publicKey", "");
        data_all.put("privateKey", "");

        KeyPair pair = KeyPair.random();
        String privateKey = new String(pair.getSecretSeed());
        String publicKey = pair.getAccountId();

        try {
            JSONObject result = addToFriendBot(publicKey);
            if (!result.getBoolean("status")) {
                data_all.put("message", "By function addToFriendBot " + result.getString("message"));
            } else {
                System.out.println("addToFriendBot Success");
            }
        } catch (Exception e) {
            data_all.put("message", "Try Catch in addToFriendBot " + e.getMessage());
            return data_all;
        }

        data_all.put("status", true);
        data_all.put("message", "success");
        data_all.put("publicKey", publicKey);
        data_all.put("privateKey", privateKey);

        return data_all;
    }

    @Override
    public JSONObject addToFriendBot(String publicKey) throws MalformedURLException, IOException {
        Network network = new Network("Betimes SDF Network ; Dec 2018");
        Network.use(network);
        Server server = new Server(ServerUrl);

        // Root Account Stellar Amarin
        KeyPair source = KeyPair.fromSecretSeed(root_acc_stellar_private_key);
        KeyPair destination = KeyPair.fromAccountId(publicKey);

        AccountResponse sourceAccount = server.accounts().account(source);
        Transaction tranCreateAccount = new Transaction.Builder(sourceAccount)
                .addOperation(new CreateAccountOperation.Builder(destination, "10000").setSourceAccount(source).build())
                .addMemo(Memo.text("Create Account")).build();
        tranCreateAccount.sign(source);

        SubmitTransactionResponse response = server.submitTransaction(tranCreateAccount);
        JSONObject obj = new JSONObject();
        if (!response.isSuccess()) {
            obj.put("message", response.getExtras().getResultCodes().getTransactionResultCode());
            obj.put("operationResult", response.getExtras().getResultCodes().getOperationsResultCodes());
            obj.put("status", false);
        } else {
            obj.put("message", "done");
            obj.put("status", true);
        }
        return obj;
    }

    @Override
    public JSONObject trustAsset(String privateIssuerKey, String privateReciveKey, String AssetName,
            String AmountHolder) throws IOException {
        Network network = new Network("Betimes SDF Network ; Dec 2018");
        Network.use(network);
        Server server = new Server(ServerUrl);

        // Keys for accounts to issue and receive the new asset
        KeyPair issuingKeys = KeyPair.fromSecretSeed(privateIssuerKey);
        // KeyPair.fromSecretSeed(privateCreateKey);
        KeyPair receivingKeys = KeyPair.fromSecretSeed(privateReciveKey);

        // Create an object to represent the new asset
        // Asset assets = Asset.createNonNativeAsset(AssetName, issuingKeys);
        Asset assets = new AssetTypeCreditAlphaNum12(AssetName, issuingKeys);

        // First, the receiving account must trust the asset
        AccountResponse receiving = server.accounts().account(receivingKeys);
        Transaction allowAssets = new Transaction.Builder(receiving).addOperation(
                // The `ChangeTrust` operation creates (or alters) a trustline
                // The second parameter limits the amount the account can hold
                new ChangeTrustOperation.Builder(assets, AmountHolder).build()).build();
        allowAssets.sign(receivingKeys);

        JSONObject obj = new JSONObject();
        SubmitTransactionResponse response = server.submitTransaction(allowAssets);
        if (!response.isSuccess()) {
            obj.put("message", response.getExtras().getResultCodes().getTransactionResultCode());
            obj.put("operationResult", response.getExtras().getResultCodes().getOperationsResultCodes());
            obj.put("getEnvelopeXdr", response.getExtras().getEnvelopeXdr());
            obj.put("status", false);
        } else {
            obj.put("message", "done");
            obj.put("status", true);
        }
        return obj;
    }

    @Override
    public JSONObject giveChoicesPoint(String walletId, String activityId, String Choices_id) throws IOException {
        String AssetName = Everything.renameEp(activityId);
        JSONObject obj = new JSONObject();
        obj.put("status", false);

        JSONObject user = Everything.jedis_lockbox(walletId);
        if (!checkObject(user)) {
            obj.put("message", user.getString("message"));
            return obj;
        }
        String privateUserKey = user.getJSONObject("data_object").getString("privateKey");

        JSONObject issuer = Everything.jedis_lockbox(activityId + "Issuer");
        if (!checkObject(issuer)) {
            obj.put("message", issuer.getString("message"));
            return obj;
        }
        String privateIssuerKey = issuer.getJSONObject("data_object").getString("privateKey");

        JSONObject dis = Everything.jedis_lockbox(activityId + "Distributor");
        if (!checkObject(dis)) {
            obj.put("message", dis.getString("message"));
            return obj;
        }
        String privateDisKey = dis.getJSONObject("data_object").getString("privateKey");

        JSONObject choices = Everything.jedis_lockbox(activityId + "Choice" + Choices_id);
        if (!checkObject(choices)) {
            obj.put("message", choices.getString("message"));
            return obj;
        }
        String privateChoicesKey = choices.getJSONObject("data_object").getString("privateKey");

        JSONObject trust = trustAsset(privateIssuerKey, privateUserKey, AssetName, "1000000");
        if (trust.getBoolean("status")) {
            JSONObject DisToUser = giveCoin(privateIssuerKey, privateDisKey, privateUserKey, AssetName);

            serviceLog.setLogCommon(AssetName, "voteChoices-topUp", walletId, activityId + "Distributor", walletId, "1",
                    DisToUser.getString("message"), DisToUser.getBoolean("status"));

            if (DisToUser.getBoolean("status")) {
                JSONObject UserToChoice = giveCoin(privateIssuerKey, privateUserKey, privateChoicesKey, AssetName);

                serviceLog.setLogCommon(AssetName, "voteChoices-reDuce", walletId, walletId,
                        activityId + "Choice" + Choices_id, "1", UserToChoice.getString("message"),
                        UserToChoice.getBoolean("status"));

                if (UserToChoice.getBoolean("status")) {
                    obj.put("message", "success");
                    obj.put("status", true);
                } else {
                    obj.put("message", UserToChoice.getString("message"));
                }
            } else {
                obj.put("message", DisToUser.getString("message"));
            }
        } else {
            obj.put("message", trust.getString("message"));
        }

        return obj;
    }

    @Override
    public JSONObject giveCoin(String privateIssuerKey, String privateSenderKey, String privateReciveKey,
            String AssetName) throws IOException {
        Network network = new Network("Betimes SDF Network ; Dec 2018");
        Network.use(network);
        Server server = new Server(ServerUrl);

        // Owner Assets
        KeyPair source = KeyPair.fromSecretSeed(privateIssuerKey);
        // Sender
        KeyPair sender = KeyPair.fromSecretSeed(privateSenderKey);
        // Destination
        KeyPair destination = KeyPair.fromSecretSeed(privateReciveKey);

        AccountResponse senderAccount = server.accounts().account(sender);
        Transaction giveCoinTx = new Transaction.Builder(senderAccount).addOperation(
                new PaymentOperation.Builder(destination, new AssetTypeCreditAlphaNum12(AssetName, source), "1")
                        .build())
                .build();
        giveCoinTx.sign(sender);

        JSONObject obj = new JSONObject();
        SubmitTransactionResponse response = server.submitTransaction(giveCoinTx);
        if (!response.isSuccess()) {
            obj.put("message", response.getExtras().getResultCodes().getTransactionResultCode());
            obj.put("operationResult", response.getExtras().getResultCodes().getOperationsResultCodes());
            obj.put("getEnvelopeXdr", response.getExtras().getEnvelopeXdr());
            obj.put("status", false);
        } else {
            obj.put("message", "done");
            obj.put("status", true);
        }

        return obj;
    }

    @Override
    public JSONObject resultActivity(String activityId, String amountOfChoices) throws IOException {
        Integer loop = Integer.parseInt(amountOfChoices);
        Network network = new Network("Betimes SDF Network ; Dec 2018");
        Network.use(network);
        Server server = new Server(ServerUrl);

        JSONObject response = new JSONObject();
        JSONArray arr = new JSONArray();
        for (Integer i = 1; i <= loop; i++) {
            JSONObject choices = Everything.jedis_lockbox(activityId + "Choice" + i);
            if (choices.getBoolean("status")) {
                String privateKey = choices.getJSONObject("data_object").getString("privateKey");

                KeyPair pair = KeyPair.fromSecretSeed(privateKey);

                AccountResponse account = server.accounts().account(pair);
                for (AccountResponse.Balance balance : account.getBalances()) {
                    String AssetName = Everything.renameEp(activityId);
                    if (AssetName.equals(balance.getAssetCode())) {
                        JSONObject obj = new JSONObject();
                        obj.put("choices", i);
                        obj.put("result", balance.getBalance());
                        arr.put(obj);
                        response.put("data_object", arr);
                    }
                }
            }
        }

        return response;
    }

    public Boolean checkObject(JSONObject obj) {
        // System.out.println(obj.getBoolean("status"));
        return obj.getBoolean("status");
    }
}