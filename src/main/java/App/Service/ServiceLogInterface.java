package App.Service;

public interface ServiceLogInterface {
        String setLogCommon(String asset, String type, String user_id, String source_id, String destination_id,
                        String amount, String message, boolean status);

        // void setLogWarning(String asset, String type, String user_id, String
        // source_id, String destination_id,
        // Integer amount, String message, boolean status);

        // void setLogError(String asset, String type, String user_id, String source_id,
        // String destination_id, Integer amount,
        // String message, boolean status);
}
