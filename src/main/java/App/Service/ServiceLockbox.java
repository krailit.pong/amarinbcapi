package App.Service;

import App.function.RestService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.util.Base64;

@Service
public class ServiceLockbox implements ServiceLockboxInterface {

    @Autowired
    private RestService rest;

    @Autowired
    private ServiceHorizonInterface Horizon;

    @Value("${server.host.lockbox}")
    private String lockboxUrl;

    @Override
    public JSONObject getEntryById(String walletId) {
        JSONObject data_all = new JSONObject();
        data_all.put("status", false);
        try {
            byte[] walletIdencoded = Base64.getEncoder().encode(walletId.getBytes());
            String entryName = new String(walletIdencoded);
            String url = lockboxUrl + "boxes/" + walletId + "/entries/" + entryName;
            JSONObject data_object = rest.get(url);
            System.out.println(url);
            if (data_object.getInt("http_code") == 200) {
                data_all.put("status", true);
                data_all.put("data_object", data_object);
            } else {
                String errors = "";
                if (data_object.has("errors")) {
                    for (int i = 0; i < data_object.getJSONArray("errors").length(); ++i) {
                        errors += data_object.getJSONArray("errors").getJSONObject(i).getString("message");
                        if (data_object.getJSONArray("errors").length() < i) {
                            errors += " , ";
                        }
                    }
                }
                data_all.put("message", "Fail to send request Get Boxes to Lockbox Detail : " + errors);
            }
        } catch (Exception e) {
            data_all.put("message", e.getMessage());
        }

        return data_all;
    }

    @Override
    public JSONObject setBoxesById(String walletId) {
        JSONObject data_all = new JSONObject();
        data_all.put("status", false);
        try {
            String url = lockboxUrl + "boxes/" + walletId;
            System.out.println(url);
            JSONObject data_object = rest.post("", url);
            // 201 Return blank body response
            // It never http code 200 because api success return 201
            if (data_object.getInt("http_code") == 201) {
                data_all.put("status", true);
            } else {
                String errors = "";
                if (data_object.has("errors")) {
                    for (int i = 0; i < data_object.getJSONArray("errors").length(); ++i) {
                        errors += data_object.getJSONArray("errors").getJSONObject(i).getString("message");
                        if (data_object.getJSONArray("errors").length() < i) {
                            errors += " , ";
                        }
                    }
                }
                data_all.put("message", "Fail to send request Create Boxes to Lockbox Detail : " + errors);
            }
        } catch (Exception e) {
            data_all.put("message", "try " + e.getMessage());
        }
        return data_all;
    }

    @Override
    public JSONObject setEntryById(JSONObject resultWallet, String walletId) {
        JSONObject data_all = new JSONObject();
        data_all.put("status", false);

        String entriesUrl = lockboxUrl + "boxes/" + walletId + "/entries";
        byte[] walletIdencoded = Base64.getEncoder().encode(walletId.toString().getBytes());
        JSONObject json = new JSONObject();
        JSONObject value = new JSONObject();
        json.put("key", new String(walletIdencoded));
        value.put("_id", walletId);
        value.put("publicKey", resultWallet.getString("publicKey"));
        value.put("privateKey", resultWallet.getString("privateKey"));
        json.put("value", value);
        JSONObject result = rest.post(json.toString(), entriesUrl);
        if (result.getInt("http_code") == 201) {
            data_all.put("status", true);
            data_all.put("message", "success");
        } else {
            data_all.put("message", "Fail To Send Request to Lockbox !");
            data_all.put("data_object", json);
        }

        return data_all;
    }
}