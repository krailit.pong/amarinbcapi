package App.Service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import App.function.ElasLog;

@Service
public class ServiceLog implements ServiceLogInterface {
    // public enum Level {
    //     COMMON(3), WARNING(2), ERROR(1);

    //     private final int levelCode;

    //     Level(int levelCode) {
    //         this.levelCode = levelCode;
    //     }

    //     public int getLevelCode() {
    //         return this.levelCode;
    //     }
    // }

    @Autowired
    private ElasLog elaslog;

    @Async
    @Override
    public String setLogCommon(String asset, String type, String user_id, String source_id, String destination_id,
            String amount, String message, boolean status) {

        // Level level = Level.COMMON;

        JSONObject json = new JSONObject();
        json.put("asset", asset);
        json.put("type", type);
        json.put("user_id", user_id);
        json.put("source_id", source_id);
        json.put("destination_id", destination_id);
        json.put("amount", amount);
        json.put("message", message);
        json.put("status", status);
        // json.put("level", level.getLevelCode());
        elaslog.post("text", json);

        return "";
    }

    // @Async
    // @Override
    // public void setLogWarning(String asset, String type, String user_id, String source_id, String destination_id,
    //         Integer amount, String message, boolean status) {

    //     Level level = Level.WARNING;

    //     JSONObject json = new JSONObject();
    //     json.put("asset", asset);
    //     json.put("type", type);
    //     json.put("user_id", user_id);
    //     json.put("source_id", source_id);
    //     json.put("destination_id", destination_id);
    //     json.put("amount", amount);
    //     json.put("message", message);
    //     json.put("status", status);
    //     // json.put("level", level.getLevelCode());
    //     elaslog.post("text", json);
    // }

    // @Async
    // @Override
    // public void setLogError(String asset, String type, String user_id, String source_id, String destination_id,
    //         Integer amount, String message, boolean status) {

    //     Level level = Level.ERROR;

    //     JSONObject json = new JSONObject();
    //     json.put("asset", asset);
    //     json.put("type", type);
    //     json.put("user_id", user_id);
    //     json.put("source_id", source_id);
    //     json.put("destination_id", destination_id);
    //     json.put("amount", amount);
    //     json.put("message", message);
    //     json.put("status", status);
    //     // json.put("level", level.getLevelCode());
    //     elaslog.post("text", json);
    // }
}