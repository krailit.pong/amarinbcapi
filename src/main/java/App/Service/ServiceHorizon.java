package App.Service;

import App.function.Everything;
import App.function.FormatSerch;
import App.function.RestService;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import org.stellar.sdk.Asset;
import org.stellar.sdk.AssetTypeNative;
import org.stellar.sdk.ChangeTrustOperation;
import org.stellar.sdk.CreateAccountOperation;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.Memo;
import org.stellar.sdk.Network;
import org.stellar.sdk.PaymentOperation;

import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.*;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.stellar.sdk.Server;
import org.stellar.sdk.Transaction;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

@Service
public class ServiceHorizon implements ServiceHorizonInterface {

    @Autowired
    private RestService rest;

    @Value("${server.host.stellar}")
    private String ServerUrl;

    @Value("${root.acc.stellar.private_key}")
    private String root_acc_stellar_private_key;

    @Override
    public JSONObject createWallet() {
        JSONObject data_all = new JSONObject();
        data_all.put("status", false);
        data_all.put("publicKey", "");
        data_all.put("privateKey", "");
        try {
            KeyPair pair = KeyPair.random();
            String getSecretSeed = new String(pair.getSecretSeed());
            String getAccountId = pair.getAccountId();

            JSONObject result = addToFriendBot(getAccountId);
            if (result.getBoolean("status")) {
                data_all.put("status", true);
                data_all.put("message", "success");
                data_all.put("publicKey", getAccountId);
                data_all.put("privateKey", getSecretSeed);
            } else {
                data_all.put("message", "By function addToFriendBot " + result.getString("message"));
            }
        } catch (Exception e) {
            data_all.put("message", "By function createWallet Try Catch " + e.getMessage());
        }
        return data_all;
    }

    @Override
    public JSONObject addToFriendBot(String publicKey) throws MalformedURLException, IOException {
        Network network = new Network("Betimes SDF Network ; Dec 2018");
        Network.use(network);
        Server server = new Server(ServerUrl);

        // Root Account Stellar Amarin
        KeyPair source = KeyPair.fromSecretSeed(root_acc_stellar_private_key);
        KeyPair destination = KeyPair.fromAccountId(publicKey);

        AccountResponse sourceAccount = server.accounts().account(source);
        Transaction tranCreateAccount = new Transaction.Builder(sourceAccount)
                .addOperation(new CreateAccountOperation.Builder(destination, "10000").setSourceAccount(source).build())
                .addMemo(Memo.text("Create Account")).build();
        tranCreateAccount.sign(source);

        SubmitTransactionResponse response = server.submitTransaction(tranCreateAccount);
        JSONObject obj = new JSONObject();
        if (!response.isSuccess()) {
            obj.put("message", response.getExtras().getResultCodes().getTransactionResultCode());
            obj.put("operationResult", response.getExtras().getResultCodes().getOperationsResultCodes());
            obj.put("status", false);
        } else {
            obj.put("message", "done");
            obj.put("status", true);
        }
        return obj;
    }

    @Override
    public JSONObject getWalletDetail(String publicKey) {
        String url = String.format(ServerUrl + "/accounts/" + publicKey);
        JSONObject call = rest.get(url);
        JSONObject obj = new JSONObject();
        obj.put("status", false);
        if (call.has("balances") && !call.isNull("balances")) {
            obj.put("status", true);
            obj.put("data_object", call.getJSONArray("balances"));
            obj.put("message", "success");
        } else {
            obj.put("message", call);
        }
        return obj;
    }
}