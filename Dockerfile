FROM frolvlad/alpine-oraclejdk8:slim

ADD AmarinBCApi-1.0-SNAPSHOT.jar app.jar
ADD application.properties application.properties
RUN sh -c 'touch /app.jar'

ENTRYPOINT ["java", "-Dspring.config.location=/App/application.properties", "-jar","/app.jar"]